function testChamps() {
	var email=document.getElementById("email").value;
	var dateRDV=document.getElementById("dateRDV").value;
	var docteur=document.getElementById("docteur").options[document.getElementById("docteur").selectedIndex].text;
	var service=document.getElementById("service").options[document.getElementById("service").selectedIndex].text;
	var now = new Date();
	var jour = ('0'+now.getDate()).slice(-2);
	var mois = ('0'+(now.getMonth()+1)).slice(-2);
	var annee = now.getFullYear();
	if (dateRDV!='' && email!='')
	{
		var pdf = { content: [ { text : [
		{ text : 'Demande de Rendez-vous', fontSize : 34, alignment : 'center'},'\n\n\n\n',
		'Demande effectuée le : ',{ text : [jour+'/'+mois+'/'+annee], decoration : 'underline' }]},
		{ text : ['Pour un rendez-vous en date de : ',{ text : dateRDV, decoration : 'underline' }], margin : [0,-14,0,0], alignment : 'right' },
		{ text : [
		'\nPar : ',{ text : [nom+' '+prenom], decoration : 'underline' },'\n\n',
		'Adresse : ',{ text : adresse, decoration : 'underline' },'\n\n',
		'Code Postal : ',{ text : CP, decoration : 'underline' },'\n\n',
		'Ville : ',{ text : ville, decoration : 'underline' },'\n\n',
		'Avec le docteur : ',{ text : docteur, decoration : 'underline' },'\n\n',
		'Service : ',{ text : service, decoration : 'underline' }]}], pageOrientation: 'landscape'
		};
		if (!!window.chrome && !!window.chrome.webstore)
		{
			pdfMake.createPdf(pdf).download('demande_de_rendez-vous.pdf');
		}
		else
		{
			pdfMake.createPdf(pdf).open();
		}
	}
	else
	{
		alert("Veuillez Indiquer votre adresse email et la date du rendez vous.");
	}
}
