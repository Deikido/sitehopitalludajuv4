function avisPdf() {
	var secteur=document.getElementById("secteur").value;
	var accueilSer=document.querySelector('input[name="accueilSer"]:checked').value;
	var accueilSoin=document.querySelector('input[name="accueilSoin"]:checked').value;
	var qualiPrise=document.querySelector('input[name="qualiPrise"]:checked').value;
	var qualiSoin=document.querySelector('input[name="qualiSoin"]:checked').value;
	var ecouteSoin=document.querySelector('input[name="ecouteSoin"]:checked').value;
	var soulagementSoin=document.querySelector('input[name="soulagementSoin"]:checked').value;
	
	if(secteur=="--Sélectionner--")
	{
		alert("Attention vous n'avez pas choisi de service !");
	}
	
	else
	{
		if(accueilSer=="Sans Avis" && accueilSoin=="Sans Avis" && qualiPrise=="Sans Avis" && qualiSoin=="Sans Avis" && ecouteSoin=="Sans Avis" && soulagementSoin=="Sans Avis")
		{
			if(confirm("Vous n'avez donné votre avis sur aucun service. Continuer?"))
			{
				var pdf = { content: [{ text :[
				{ text : 'Formulaire de Satisfaction', fontSize : 34, alignment : 'center'},'\n\n\n\n',
				'Vous étiez dans le service : ',{text : secteur, decoration : 'underline'},'\n',
				'L\' accueil au service adminstratif a été : ',{text : accueilSer, decoration : 'underline'},'\n',
				'L\' accueil dans l\'unité de soin a été : ',{text : accueilSoin, decoration : 'underline'},'\n',
				'La qualité de prise en charge par les médecins a été : ',{text : qualiPrise, decoration : 'underline'},'\n',
				'La qualité de prise en charge par les infirmiers a été : ',{text : qualiSoin, decoration : 'underline'},'\n',
				'L\' écoute du service a été : ',{text : ecouteSoin, decoration : 'underline'},'\n',
				'Le soulagement a été : ',{text : soulagementSoin, decoration : 'underline'}]}]};
				pdfMake.createPdf(pdf).open();
			}
			else
			{
				alert("Dans ce cas, ressaisissez");
			}
		}
		else
		{
			var pdf = { content: ['L\'accueil au service adminstratif a été : '+accueilSer, 'L\'accueil dans l\'unité de soin a été : '+accueilSoin, 'La qualité de prise en charge par les médecins a été : '+qualiPrise,'La qualité de prise en charge par les infirmiers a été : '+qualiSoin,'L\'écoute du service a été : '+ecouteSoin,'Le soulagement a été : '+soulagementSoin] };
			pdfMake.createPdf(pdf).open();
		}
	}
	
}