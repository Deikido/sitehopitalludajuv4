-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 15 Mai 2017 à 13:06
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd_hopital_ludaju`
--

-- --------------------------------------------------------

--
-- Structure de la table `rdv`
--

CREATE TABLE `rdv` (
  `idrdv` int(11) NOT NULL,
  `dateheurerdv` datetime NOT NULL,
  `datepriserdv` datetime NOT NULL,
  `emailPatient` varchar(50) NOT NULL,
  `emailMedecin` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `service`
--

CREATE TABLE `service` (
  `codeservice` char(5) NOT NULL,
  `libelle` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `service`
--

INSERT INTO `service` (`codeservice`, `libelle`) VALUES
('CDLOG', 'Cardiologie'),
('CHAMB', 'Chirurgie ambulatoire'),
('CHDIG', 'Chirurgie digestive'),
('CHORT', 'Chirurge ortho/trauma'),
('DMTLG', 'Dermatologie'),
('ENLOG', 'Endocrinologie'),
('MEDGR', 'Médecine gériatrique'),
('NEUGI', 'Neurogirurgie'),
('NPLOG', 'Nephrologie'),
('NRLOG', 'Neurologie'),
('URGEN', 'Urgences');

-- --------------------------------------------------------

--
-- Structure de la table `typeutil`
--

CREATE TABLE `typeutil` (
  `codeTypeUtil` int(11) NOT NULL,
  `libelle` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `typeutil`
--

INSERT INTO `typeutil` (`codeTypeUtil`, `libelle`) VALUES
(1, 'patient'),
(2, 'medecin'),
(3, 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `email` varchar(50) NOT NULL,
  `mdp` char(64) NOT NULL,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `tel` char(10) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `cp` char(5) DEFAULT NULL,
  `ville` varchar(25) DEFAULT NULL,
  `codeservice` char(5) DEFAULT NULL,
  `codeTypeUtil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`email`, `mdp`, `nom`, `prenom`, `tel`, `adresse`, `cp`, `ville`, `codeservice`, `codeTypeUtil`) VALUES
('alexandrin.pitre@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Pitre', 'Alexandrin', NULL, NULL, NULL, NULL, 'URGEN', 2),
('ancelina.lussier@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Lussier', 'Ancelina', NULL, NULL, NULL, NULL, 'CDLOG', 2),
('bevis.fouquet@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Fouquet', 'Bevis', NULL, NULL, NULL, NULL, 'NEUGI', 2),
('bruce.dionne@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Dionne', 'Bruce', NULL, NULL, NULL, NULL, 'NEUGI', 2),
('chappell.carriere@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Carrière', 'Chappell', NULL, NULL, NULL, NULL, 'ENLOG', 2),
('christophe.lemelin@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Lemelin', 'Christophe', NULL, NULL, NULL, NULL, 'MEDGR', 2),
('clovis.croteau@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Croteau', 'Clovis', NULL, NULL, NULL, NULL, 'URGEN', 2),
('coralie.monty@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Monty', 'Coralie', NULL, NULL, NULL, NULL, 'URGEN', 2),
('david.ivain@gmail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Ivain', 'David', '0000000000', 'qqpart', '59100', 'Roubaix', NULL, 1),
('didier.pellerin@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Pellerin', 'Didier', NULL, NULL, NULL, NULL, 'NPLOG', 2),
('etoile.beriault@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Bériault', 'Etoile', NULL, NULL, NULL, NULL, 'CHORT', 2),
('ferrau.bordeleau@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Bordeleau', 'Ferrau', NULL, NULL, NULL, NULL, 'CHAMB', 2),
('harcourt.potvin@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Potvin', 'Harcourt', NULL, NULL, NULL, NULL, 'MEDGR', 2),
('henri.rouze@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Rouze', 'Henri', NULL, NULL, NULL, NULL, 'NEUGI', 2),
('honore.garreau@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Garreau', 'Honoré', NULL, NULL, NULL, NULL, 'NEUGI', 2),
('jay.faucher@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Faucher', 'Jay', NULL, NULL, NULL, NULL, 'CHDIG', 2),
('jay.st-jean@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'St-Jean', 'Jay', NULL, NULL, NULL, NULL, 'URGEN', 2),
('jullesang@gmail.com', '', 'Le-Sang', 'Jul', '0000000000', 'ici', '59100', 'Roubaix', NULL, 1),
('laure.charest@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Charest', 'Laure', NULL, NULL, NULL, NULL, 'CHAMB', 2),
('laurent.fluet@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Fluet', 'Laurent', NULL, NULL, NULL, NULL, 'CHDIG', 2),
('laverne.brian@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Brian', 'Laverne', NULL, NULL, NULL, NULL, 'ENLOG', 2),
('masson.drouin@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Drouin', 'Masson', NULL, NULL, NULL, NULL, 'DMTLG', 2),
('nathalie.roux@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Roux', 'Nathalie', NULL, NULL, NULL, NULL, 'ENLOG', 2),
('ouioui@gmail.com', '728b252625ebcddcea74d61760866080a10196087c340a57a88ba511bd387921', 'Oui', 'Oui', '0000000000', 'qqpart', '59100', 'Roubaix', NULL, 1),
('patrick.patel@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Patel', 'Patrick', NULL, NULL, NULL, NULL, 'CHAMB', 2),
('pauline.levasseur@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Levasseur', 'Pauline', NULL, NULL, NULL, NULL, 'CDLOG', 2),
('romaine.leclerc@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Leclerc', 'Romaine', NULL, NULL, NULL, NULL, 'NRLOG', 2),
('royden.sansouci@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Sansouci', 'Royden', NULL, NULL, NULL, NULL, 'CHORT', 2),
('sebastian.castellanos@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Castellanos', 'Sebastian', NULL, NULL, NULL, NULL, 'CDLOG', 2),
('thibaut.ducharme@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Ducharme', 'Thibaut', NULL, NULL, NULL, NULL, 'URGEN', 2),
('victoire.levasseur@mail.com', 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'Levasseur', 'Victoire', NULL, NULL, NULL, NULL, 'NRLOG', 2);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `rdv`
--
ALTER TABLE `rdv`
  ADD PRIMARY KEY (`idrdv`),
  ADD KEY `idpatient` (`emailPatient`),
  ADD KEY `idmedecin` (`emailMedecin`);

--
-- Index pour la table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`codeservice`);

--
-- Index pour la table `typeutil`
--
ALTER TABLE `typeutil`
  ADD PRIMARY KEY (`codeTypeUtil`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`email`),
  ADD KEY `codeTypeUtil` (`codeTypeUtil`),
  ADD KEY `codeservice` (`codeservice`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `rdv`
--
ALTER TABLE `rdv`
  MODIFY `idrdv` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `rdv`
--
ALTER TABLE `rdv`
  ADD CONSTRAINT `rdv_ibfk_1` FOREIGN KEY (`emailPatient`) REFERENCES `utilisateur` (`email`),
  ADD CONSTRAINT `rdv_ibfk_2` FOREIGN KEY (`emailMedecin`) REFERENCES `utilisateur` (`email`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`codeservice`) REFERENCES `service` (`codeservice`),
  ADD CONSTRAINT `utilisateur_ibfk_2` FOREIGN KEY (`codeTypeUtil`) REFERENCES `typeutil` (`codeTypeUtil`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
