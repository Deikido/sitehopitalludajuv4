<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Demande de rendez-vous</title>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../../style.css"/>
        <script src='../../build/pdfmake.min.js'></script>
        <script src='../../build/vfs_fonts.js'></script>
        <script src="../../build/creationpdfrdv.js"></script>
        <script src="../../build/listedocteursservices.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    </head>
    <body>
        <header>
            <div id="titre">H<a href="http://humourtop.com/les-meilleurs-gifs-droles-de-2013/Simba_dead.gif" id="important">ô</a>pital LuDaJu</div>
            <div id="connexion">
                <ul>
                    <?php
			if (!isset($_SESSION['email'])) {
				echo '<li>
                        <a href="../../authentification/login.php" id="test1">Se connecter</a>
                    </li>
                    <li>
                        <a href="enregistrement.php" id="test2">S\'inscrire</a>
                    </li>';
			}
			else {
                            if($_SESSION["codeTypeUtil"]==1){
                                echo '<li>
                        <a id="test1" href="listeRdvPatient.php">' . $_SESSION['prenom'] . '</a>';
                            }
                            else{
                                echo '<li>
                        <a id="test1" href="listeRdvMedecin.php">' . $_SESSION['prenom'] . '</a>';
                            }
				
                    echo '</li>
                    <li>
                        <a href="../../authentification/traitementDeco.php" id="test2">Se déconnecter</a>
                    </li>';
			}
			?>
                </ul>
            </div>

        </header>

        <?php
        include("../test/mesFonctions.php");
        echo menu();
        ?>
        <div id="contenu">
            <h1 id="enTete">Demande de Rendez-vous</h1>
            <?php
            if ($_SESSION['codeTypeUtil'] == 1) {
                echo '<form id="formulaire" action="priseRdvPatient.php" method="post">
                <label for="date">Date du rendez vous (aaaa-mm-jj) : </label><input type="date" name="dateRDV" id="dateRDV" required/><br>
                <label for="heure">Heure : </label><select name="heure" id="heure">
                    <option value="08:00:00">08:00</option>
                    <option value="09:00:00">09:00</option>
                    <option value="10:00:00">10:00</option>
                    <option value="13:00:00">13:00</option>
                    <option value="14:00:00">14:00</option>
                    <option value="15:00:00">15:00</option>
                    <option value="16:00:00">16:00</option>
                    <option value="17:00:00">17:00</option>
                </select><br>
                <!--<label for="email">Email : </label><input type="text" name="email" id="email" required/> <a href="enregistrement.php" title="Vous enregistrer">Première visite ? Cliquez ici !</a><br>-->
                <!--<label for="prenom">Prénom : </label><input type="text" name="prenom" id="prenom" required/><br>
                <label for="adresse">Adresse : </label><input type="text" name="adresse" id="adresse" required/><br>
                <label for="CP">Code Postal : </label><input type="text" name="CP" id="CP" required/><br>
                <label for="ville">Ville : </label><input type="text" name="ville" id="ville" required/><br>
                <label for="tel">Telephone : </label><input type="text" name="tel" id="tel" required/><br>-->
                <label for="service">Service : </label><select name="service" id="service" onchange="liste(medecins)">';
                $objPdo = Connexion();
                $services = chargerListeServices($objPdo);
                foreach ($services as $unService) {
                    echo '<option value="' . $unService["codeservice"] . '">' . $unService["libelle"] . '</option>';
                }
                echo '</select><br>
                <label for="docteur">Docteur : </label><select name="docteur" id="docteur">
                </select><br>
                <button type="submit" onclick="" id="confirmer"/>Confirmer</button>
                <input type="reset" name="annuler" value="Réinitialiser" />
                <div>Liste de vos <a href="listeRdvPatient.php">rendez-vous</a></div>
            </form>';
            } else {
                echo 'Veuillez <a href="../../authentification/login.php">vous connecter</a> ou <a href="enregistrement.php">vous inscrire</a> pour prendre un rendez-vous.';
            }
            ?>
        </div>
        <script>
            var medecins = <?php echo json_encode(chargerListeMedecins($objPdo)); ?>;
            liste(medecins);
        </script>
        <footer>
            <center>
                <table>
                    <tr><th class="foot">Facebook</th><th class="foot">Twitter</th><th class="foot">Google+</th></tr>
                    <tr><th class="tdFoot"><a href="https://fr-fr.facebook.com/"><img class="test" src="../../images/facebook.png"/></a></th><th class="tdFoot"><a href="https://twitter.com/?lang=fr"><img class="test" src="../../images/twitter.png"/></a></th><th class="tdFoot"><a href="https://media.tenor.co/images/1f034d4f7d72a87a3167aff1395d5143/tenor.gif"><img class="test" src="../../images/google.png"/></a></th></tr>
                </table>
                <div>© 2017 LuDaJu. Tous droits réservés.</div>
            </center>
        </footer>
    </body>
</html>
