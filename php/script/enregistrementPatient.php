<?php

session_start();
include "../test/mesFonctions.php";
include "../test/fonctionsAuthentification.php";
$monPdo = Connexion();
$stmt = $monPdo->prepare("insert into utilisateur (email, nom, prenom, adresse, cp, ville, tel, mdp, codeTypeUtil) values (:email, :nom, :prenom, :adresse, :cp, :ville, :tel, :mdp, 1);");
try {
    $email = $_POST["email"];
    $nom = $_POST["nom"];
    $prenom = $_POST["prenom"];
    $adresse = $_POST["adresse"];
    $CP = $_POST["CP"];
    $ville = $_POST["ville"];
    $tel = $_POST["tel"];
    $mdp = hash('sha256', $_POST["mdp"]);
    $stmt->bindParam(":email", $email);
    $stmt->bindParam(":nom", $nom);
    $stmt->bindParam(":prenom", $prenom);
    $stmt->bindParam(":adresse", $adresse);
    $stmt->bindParam(":cp", $CP);
    $stmt->bindParam(":ville", $ville);
    $stmt->bindParam(":tel", $tel);
    $stmt->bindParam(":mdp", $mdp);
    $resultat = $stmt->execute();
    if ($resultat) {
        Redirect("demandeRendezVous.php");
    } else {
        echo '<script>alert("Erreur lors de l\'enregistrement dans la bdd")</script>';
        Redirect("enregistrement.php");
    }
} catch (Exception $e) {
    echo '<script>alert("Erreur : ' . $e->getMessage() . '")</script>';
    Redirect("enregistrement.php");
}
?>