﻿<?php
session_start();
include_once ("../test/mesFonctions.php");
$monPdo = Connexion();
$stmt = $monPdo->prepare("insert into rdv (dateheurerdv, datepriserdv, emailPatient, emailMedecin) values (:dateheurerdv, :datepriserdv, :emailPatient, :emailMedecin);");
try {
    $emailMedecin = $_POST["docteur"];
    $dateHeureRdv = $_POST["dateRDV"] . ' ' . $_POST["heure"];
    //verification horraire inachevée
    $stmt2 = $monPdo->prepare("select * from rdv where dateheurerdv = :dateHeureRdv AND emailMedecin = :emailMedecin");
    $stmt2->bindParam(":dateHeureRdv", $dateHeureRdv);
    $stmt2->bindParam(":emailMedecin", $emailMedecin);
    $verifDispo = $stmt2->fetch();
    $stmt2->closeCursor();
    if (count($verifDispo) == 0) {
        echo "c'est bon";
        $email = $_SESSION["email"];
        $datePriseRdv = date('Y-m-d H:i:s');
        $stmt->bindParam(":emailPatient", $email);
        $stmt->bindParam(":dateheurerdv", $dateHeureRdv);
        $stmt->bindParam(":datepriserdv", $datePriseRdv);
        $stmt->bindParam(":emailMedecin", $emailMedecin);
        $resultat = $stmt->execute();
        if ($resultat) {
            Redirect("confirmationRendezVous.php");
        } else {
            echo '<script>alert("Erreur lors de l\'enregistrement dans la bdd")</script>';
            Redirect("demandeRendezVous.php");
        }
    }
    else{
        echo '<script>alert("L\'heure de rendez-vous est déjà prise")</script>';
        echo '<a href="demandeRendezVous.php">Effectuer une nouvelle demande</a>';
    }

    /* }
      else
      {
      echo '<script>alert("Cet horraire est déja pris)</script>';
      Redirect("../../demandeRendezVous.php");
      } */
} catch (Exception $e) {
    echo '<script>alert("Erreur : ' . $e->getMessage() . '")</script>';
    Redirect("demandeRendezVous.php");
}
?>