<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Cardiologie</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../style.css"/>
        <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 
    </head>
    <body>
        <header>
            <div id="titre">H<a href="http://humourtop.com/les-meilleurs-gifs-droles-de-2013/Simba_dead.gif" id="important">ô</a>pital LuDaJu</div>
            <div id="connexion">
                <ul>
                    <?php
			if (!isset($_SESSION['email'])) {
				echo '<li>
                        <a href="../../authentification/login.php" id="test1">Se connecter</a>
                    </li>
                    <li>
                        <a href="enregistrement.php" id="test2">S\'inscrire</a>
                    </li>';
			}
			else {
                            if($_SESSION["codeTypeUtil"]==1){
                                echo '<li>
                        <a id="test1" href="listeRdvPatient.php">' . $_SESSION['prenom'] . '</a>';
                            }
                            else{
                                echo '<li>
                        <a id="test1" href="listeRdvMedecin.php">' . $_SESSION['prenom'] . '</a>';
                            }
				
                    echo '</li>
                    <li>
                        <a href="../../authentification/traitementDeco.php" id="test2">Se déconnecter</a>
                    </li>';
			}
			?>
                </ul>
            </div>

        </header>

        <?php
        include("../test/mesFonctions.php");
        echo menu();
        ?>
        <div id="contenu">
            <h1 id="enTete">Urgences</h1>
            <h1>Vous êtes Sur la page des Urgences</h1>
            <p>Vous trouverez ici toutes les informations utiles pour contacter ou aller jusqu'aux Urgences.</p>
            <img id="image_pansement" src="../../images/pansement.jpg"/>
            <p>
            <ul>
                <li><strong>un secteur d'acceuil</strong> qui permet à l’infirmière d’orientation et d’accueil à partir de l’interrogatoire initial et de prise de constantes, de définir le choix de l’orientation la mieux adaptée au malade et sa symptomatologie.</li>
                <li><strong>un circuit court adulte</strong> prévu pour accueillir des malades ou des blessés devant bénéficier d’immobilisation de membre, des sutures ne nécessitant pas d’hospitalisation.</li>
                <li><strong>un secteur d'hospitalisation</strong> permettant une surveillance clinique de courte durée : 20 lits, 10 lits porte et 2 salles de déchoquage.</li>
                <li><strong>un secteur d'urgences pédiatriques</strong> prévu pour accueillir des enfants âgés jusqu’à 15 ans et 3 mois maximum : 2 salles de déchoquage</li>
            </ul>
        </p>
        <p>
            <img id="image_urgence" src="../../images/urgence.jpg"/>
            Le service dispose également d’une équipe mobile gériatrique, d’une infirmière psy de 8 heures à 20 heures, d’un psychiatre bi-quotidiennement, en partenariat avec EPSMAL (Établissement Public de Santé Mentale Agglomération Lilloise), d’une équipe mobile d’addictologie et d’une équipe mobile de Réécoute Suicidant.	
        </p>
        <p>
            Le service travaille en partenariat avec la Maison de Santé, qui permet de rencontrer un médecin de ville de 20h à minuit du lundi au vendredi ainsi que le week-end.
        </p>
    </div>
    <footer>
        <center>
            <table>
                <tr><th class="foot">Facebook</th><th class="foot">Twitter</th><th class="foot">Google+</th></tr>
                <tr><th class="tdFoot"><a href="https://fr-fr.facebook.com/"><img class="test" src="../../images/facebook.png"/></a></th><th class="tdFoot"><a href="https://twitter.com/?lang=fr"><img class="test" src="../../images/twitter.png"/></a></th><th class="tdFoot"><a href="https://media.tenor.co/images/1f034d4f7d72a87a3167aff1395d5143/tenor.gif"><img class="test" src="../../images/google.png"/></a></th></tr>
            </table>
            <div>© 2017 LuDaJu. Tous droits réservés.</div>
        </center>
    </footer>
</body>
</html>
