<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Médecine</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../style.css"/>
        <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 
    </head>
    <body>
        <header>
            <div id="titre">H<a href="http://humourtop.com/les-meilleurs-gifs-droles-de-2013/Simba_dead.gif" id="important">ô</a>pital LuDaJu</div>
            <div id="connexion">
                <ul>
                    <?php
			if (!isset($_SESSION['email'])) {
				echo '<li>
                        <a href="../../authentification/login.php" id="test1">Se connecter</a>
                    </li>
                    <li>
                        <a href="enregistrement.php" id="test2">S\'inscrire</a>
                    </li>';
			}
			else {
                            if($_SESSION["codeTypeUtil"]==1){
                                echo '<li>
                        <a id="test1" href="listeRdvPatient.php">' . $_SESSION['prenom'] . '</a>';
                            }
                            else{
                                echo '<li>
                        <a id="test1" href="listeRdvMedecin.php">' . $_SESSION['prenom'] . '</a>';
                            }
				
                    echo '</li>
                    <li>
                        <a href="../../authentification/traitementDeco.php" id="test2">Se déconnecter</a>
                    </li>';
			}
			?>
                </ul>
            </div>

        </header>
        <?php
        include("../test/mesFonctions.php");
        echo menu();
        ?>

        <div id="contenu">
            <h1 id="enTete">Médecine</h1>
            <h2 id="enValeur">Les 3 services principaux de notre Hopital :</h2>
            <ul>
                <li><a href="Dermatologie.php">Dermatologie</a></li>
                <li><a href="Endocrinologie.php">Endocrinologie</a></li>
                <li><a href="Geriatrique.php">Médecine gériatrique</a></li>
            </ul>

            <table>
                <tr>
                    <th>Soignants en Médecine</th>
                </tr>
                <tr>
                    <td>Docteur Karim GALLOUJ</td>
                </tr>
                <tr>
                    <td>Docteur Giorgio DE BENEDETTO</td>
                </tr>
                <tr>
                    <td>Docteur Sabine DAMBRICOURT </td>
                </tr>
                <tr>
                    <td>Docteur Andréa LIZIO</td>
                </tr>
                <tr>
                    <td>Docteur Marie CAZAUBIEL</td>
                </tr>
                <tr>
                    <td>Docteur Anne-Sophie BALAVOINE</td>
                </tr>
                <tr>
                    <td>Docteur Emilie PARENT</td>
                </tr>
                <tr>
                    <td>Docteur Clémentine TRINEL</td>
                </tr>
                <tr>
                    <td>Docteur Françoise CARPENTIER</td>
                </tr>
                <tr>
                    <td>Docteur Jacqeline DANGLETERRE</td>
                </tr>
                <tr>
                    <td>Docteur Anne VANDENBON</td>
                </tr>
            </table>
        </div>
        <footer>
            <center>
                <table>
                    <tr><th class="foot">Facebook</th><th class="foot">Twitter</th><th class="foot">Google+</th></tr>
                    <tr><th class="tdFoot"><a href="https://fr-fr.facebook.com/"><img class="test" src="../../images/facebook.png"/></a></th><th class="tdFoot"><a href="https://twitter.com/?lang=fr"><img class="test" src="../../images/twitter.png"/></a></th><th class="tdFoot"><a href="https://media.tenor.co/images/1f034d4f7d72a87a3167aff1395d5143/tenor.gif"><img class="test" src="../../images/google.png"/></a></th></tr>
                </table>
                <div>© 2017 LuDaJu. Tous droits réservés.</div>
            </center>
        </footer>
    </body>
</html>
