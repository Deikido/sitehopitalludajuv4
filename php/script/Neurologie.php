<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Neurologie</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../style.css"/>
        <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 
    </head>
    <body>
        <header>
            <div id="titre">H<a href="http://humourtop.com/les-meilleurs-gifs-droles-de-2013/Simba_dead.gif" id="important">ô</a>pital LuDaJu</div>
            <div id="connexion">
                <ul>
                    <?php
			if (!isset($_SESSION['email'])) {
				echo '<li>
                        <a href="../../authentification/login.php" id="test1">Se connecter</a>
                    </li>
                    <li>
                        <a href="enregistrement.php" id="test2">S\'inscrire</a>
                    </li>';
			}
			else {
                            if($_SESSION["codeTypeUtil"]==1){
                                echo '<li>
                        <a id="test1" href="listeRdvPatient.php">' . $_SESSION['prenom'] . '</a>';
                            }
                            else{
                                echo '<li>
                        <a id="test1" href="listeRdvMedecin.php">' . $_SESSION['prenom'] . '</a>';
                            }
				
                    echo '</li>
                    <li>
                        <a href="../../authentification/traitementDeco.php" id="test2">Se déconnecter</a>
                    </li>';
			}
			?>
                </ul>
            </div>

        </header>
        <?php
        include("../test/mesFonctions.php");
        echo menu();
        ?>
        <div id="contenu">
            <h1 id="enTete">Unité de soins Neurologiques</h1>
            <p>secrétariat hospitalisations : 03.20.69.45.07<br />secrétariat consultations : 03.20.69.45.99</p>
            <p>Jours de consultation :<br />Consultations neurologiques : lundi, mardi, jeudi toute la journée, vendredi après-midi<br />EEG adultes: lundi matin et vendredi matin<br />EEG enfants : mercredi après-midi et jeudi matin<br />EMG : lundi matin, mardi matin et vendredi matin</p>
            <table>
                <tr>
                    <th>Soignants en Neurologie</th>
                </tr>
                <tr>
                    <td>Docteur Adeline PIERRE</td>
                </tr>
                <tr>
                    <td>Docteur Guillaume GRIMBERT</td>
                </tr>
                <tr>
                    <td>Docteur Marie-Hélène BERNACZYK</td>
                </tr>
                <tr>
                    <td>Docteur Isabelle BODIN</td>
                </tr>
                <tr>
                    <td>Docteur Serge DOAN POUESSEL</td>
                </tr>
                <tr>
                    <td>Docteur Adrian DEGARDIN</td>
                </tr>
            </table>
        </div>
        <footer>
            <center>
                <table>
                    <tr><th class="foot">Facebook</th><th class="foot">Twitter</th><th class="foot">Google+</th></tr>
                    <tr><th class="tdFoot"><a href="https://fr-fr.facebook.com/"><img class="test" src="../../images/facebook.png"/></a></th><th class="tdFoot"><a href="https://twitter.com/?lang=fr"><img class="test" src="../../images/twitter.png"/></a></th><th class="tdFoot"><a href="https://media.tenor.co/images/1f034d4f7d72a87a3167aff1395d5143/tenor.gif"><img class="test" src="../../images/google.png"/></a></th></tr>
                </table>
                <div>© 2017 LuDaJu. Tous droits réservés.</div>
            </center>
        </footer>
    </body>
</html>













