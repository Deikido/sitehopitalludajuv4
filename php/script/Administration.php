<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Administration</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../style.css"/>
        <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 
    </head>
    <body>
        <header>
            <div id="titre">H<a href="http://humourtop.com/les-meilleurs-gifs-droles-de-2013/Simba_dead.gif" id="important">ô</a>pital LuDaJu</div>
            <div id="connexion">
                <ul>
                    <?php
			if (!isset($_SESSION['email'])) {
				echo '<li>
                        <a href="../../authentification/login.php" id="test1">Se connecter</a>
                    </li>
                    <li>
                        <a href="enregistrement.php" id="test2">S\'inscrire</a>
                    </li>';
			}
			else {
                            if($_SESSION["codeTypeUtil"]==1){
                                echo '<li>
                        <a id="test1" href="listeRdvPatient.php">' . $_SESSION['prenom'] . '</a>';
                            }
                            else{
                                echo '<li>
                        <a id="test1" href="listeRdvMedecin.php">' . $_SESSION['prenom'] . '</a>';
                            }
				
                    echo '</li>
                    <li>
                        <a href="../../authentification/traitementDeco.php" id="test2">Se déconnecter</a>
                    </li>';
			}
			?>
                </ul>
            </div>

        </header>

        <?php
        include("../test/mesFonctions.php");
        echo menu();
        ?>
        <div id="contenu">
            <h1 id="enTete">Formalités Administratives</h1>
            <p>Les consultations sont assurées uniquement sur rendez-vous.<br>Le Bureau Central des Rendez-Vous (BCRV) a pour mission de vous donner satisfaction en planifiant des rendez-vous pour la plus grande majorité des consultations et examens spécialisés tout en respectant les consignes médicales. Toutefois, des impondérables (urgences, interventions chirurgicales) peuvent occasionner des retards dans le déroulement des consultations.<br>Lors de votre arrivée aux consultations, vous devez présenter votre pièce d’identité et selon votre situation :</p>
            <ul>
                <li>Votre carte vitale, carte mutuelle, l’attestation remise par votre centre de sécurité sociale si vous êtes bénéficiaire de la CMU, la CMC ou de l’AME, votre carte européenne de santé, l’imprimé E112.</li>
                <li>Selon votre cas, le volet 2 de déclaration d’accident du travail, la feuille de suivi maternité, le carnet de soins pour les bénéficiaires de l’article L115 du code des pensions militaires.</li>
            </ul>
            <p>Le ticket modérateur varie entre 10% et 40% en fonction du type d’examens et de la caisse d’assurance maladie.<br>Les consultations sont payables à l’avance.</p>
            <p>Pour toute consultation ou examen spécialisé (radiographies, échographies, scanner) et certaines consultations privées, veuillez vous présenter à la caisse des consultations externes : une fiche de circulation vous sera remise ; elle vous permettra d’effectuer le paiement de votre consultation ou de votre examen à la sortie.</p>
            <p>Les agents du service de la caisse des consultations externes vous orienteront vers le lieu de consultation ou examen.</p>
            <p>Le BCRV est ouvert du lundi au vendredi de 8h30 à 18h<br>Tél : 01 30 22 43 83</p>
        </div>
        <footer>
            <center>
                <table>
                    <tr><th class="foot">Facebook</th><th class="foot">Twitter</th><th class="foot">Google+</th></tr>
                    <tr><th class="tdFoot"><a href="https://fr-fr.facebook.com/"><img class="test" src="../../images/facebook.png"/></a></th><th class="tdFoot"><a href="https://twitter.com/?lang=fr"><img class="test" src="../../images/twitter.png"/></a></th><th class="tdFoot"><a href="https://media.tenor.co/images/1f034d4f7d72a87a3167aff1395d5143/tenor.gif"><img class="test" src="../../images/google.png"/></a></th></tr>
                </table>
                <div>© 2017 LuDaJu. Tous droits réservés.</div>
            </center>
        </footer>
    </body>
</html>
