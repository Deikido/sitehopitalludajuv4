<?php session_start(); ?>
<html>
    <head>
        <title>Formulaire</title>
        <meta charset="UTF-8">
        <script src="../../build/creationPdfAvis.js"></script>
        <script src='../../build/pdfmake.min.js'></script>
        <script src='../../build/vfs_fonts.js'></script>
        <link rel="stylesheet" type="text/css" href="../../style.css"/>
    </head>
    <body>
        <form>
            <center>
                <table id="tabAvis">
                    <tr>
                        <th style="text-align: left;">Séjour dans le service :</th>
                        <th>
                    <form>
                        <select name="secteur" id="secteur">
                            <option>--Sélectionner--
                            <option>Chirurgie</option>
                            <option>Pédiatrie</option>
                            <option>Neurologie</option>
                            <option>Médecine</option>
                            <option>Urgences</option>
                        </select>
                    </form>
                    </th>
                    </tr>
                    <tr>
                        <th style="text-align: left;">Accueil:</th>
                        <th class="notion">Très bien</th>
                        <th class="notion">Bien</th>
                        <th class="notion">Moyen</th>
                        <th class="notion">Mauvais</th>
                        <th class="notion">Pas d'avis</th>
                    </tr>
                    <tr>
                        <th style="text-indent: 20px; text-align: left;">Accueil du service administratif :</th>
                        <th><input type="radio" name="accueilSer" value="Tres Bien"/></th>
                        <th><input type="radio" name="accueilSer" value="Bien"/></th>
                        <th><input type="radio" name="accueilSer" value="Moyen"/></th>
                        <th><input type="radio" name="accueilSer" value="Mauvais"/></th>
                        <th><input type="radio" name="accueilSer" value="Sans Avis" checked="true"/></th>
                    </tr>
                    <tr>
                        <th style="text-indent: 20px; text-align: left;">Accueil dans l'unité de soin:</th>
                        <th><input type="radio" name="accueilSoin" value="Tres Bien"/></th>
                        <th><input type="radio" name="accueilSoin" value="Bien"/></th>
                        <th><input type="radio" name="accueilSoin" value="Moyen"/></th>
                        <th><input type="radio" name="accueilSoin" value="Mauvais"></th>
                        <th><input type="radio" name="accueilSoin" value="Sans Avis" checked="true"/></th>
                    </tr>
                    <tr>
                        <th><br/></th>
                    </tr>
                    <tr>
                        <th style="text-align: left;">Qualité de:</th>
                        <th class="notion">Très bien</th>
                        <th class="notion">Bien</th>
                        <th class="notion">Moyen</th>
                        <th class="notion">Mauvais</th>
                        <th class="notion">Pas d'avis</th>
                    </tr>
                    <tr>
                        <th style="text-indent: 20px; text-align: left;">Prise en charge des médecins :</th>
                        <th><input type="radio" name="qualiPrise" value="Tres Bien"/></th>
                        <th><input type="radio" name="qualiPrise" value="Bien"/></th>
                        <th><input type="radio" name="qualiPrise" value="Moyen"/></th>
                        <th><input type="radio" name="qualiPrise" value="Mauvais"/></th>
                        <th><input type="radio" name="qualiPrise" value="Sans Avis" checked="true"/></th>
                    </tr>
                    <tr>
                        <th style="text-indent: 20px; text-align: left;">Soins (infirmiers-aides soignants):</th>
                        <th><input type="radio" name="qualiSoin" value="Tres Bien"/></th>
                        <th><input type="radio" name="qualiSoin" value="Bien"/></th>
                        <th><input type="radio" name="qualiSoin" value="Moyen"/></th>
                        <th><input type="radio" name="qualiSoin" value="Mauvais"/></th>
                        <th><input type="radio" name="qualiSoin" value="Sans Avis" checked="true"/></th>
                    </tr>
                    <th><br/></th>
                    </tr>
                    <tr>
                        <th style="text-align: left;">Prise en charge de la douleur:</th>
                        <th class="notion">Très bien</th>
                        <th class="notion">Bien</th>
                        <th class="notion">Moyen</th>
                        <th class="notion">Mauvais</th>
                        <th class="notion">Pas d'avis</th>
                    </tr>
                    <tr>
                        <th style="text-indent: 20px; text-align: left;">A-t-on été à votre écoute?:</th>
                        <th><input type="radio" name="ecouteSoin" value="Tres Bien"/></th>
                        <th><input type="radio" name="ecouteSoin" value="Bien"/></th>
                        <th><input type="radio" name="ecouteSoin" value="Moyen"/></th>
                        <th><input type="radio" name="ecouteSoin" value="Mauvais"/></th>
                        <th><input type="radio" name="ecouteSoin" value="Sans Avis" checked="true"/></th>
                    </tr>
                    <tr>
                        <th style="text-indent: 20px; text-align: left;">Le soulagement de la douleur a été?:</th>
                        <th><input type="radio" name="soulagementSoin" value="Tres Bien"/></th>
                        <th><input type="radio" name="soulagementSoin" value="Bien"/></th>
                        <th><input type="radio" name="soulagementSoin" value="Moyen"/></th>
                        <th><input type="radio" name="soulagementSoin" value="Mauvais"/></th>
                        <th><input type="radio" name="soulagementSoin" value="Sans Avis" checked="true"/></th>
                    </tr>
                    <tr>
                        <th><input type="reset" name="Annuler" value="Annuler"></th>
                    </tr>
                </table>
        </form>
        <button type="button" onclick="avisPdf()" id="confirmer"/>Confirmer</button><a href="../../index.php"><button type="button"/>Accueil</button></a>
</center>
</body>
</html>