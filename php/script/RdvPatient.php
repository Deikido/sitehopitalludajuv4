<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Liste des Rendez-vous Patient</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../style.css"/>
    </head>
    <body>
        <div id='titre'>
            <h1>Hôpital LuDaJu</h1>
            <ul id="connexion">
                <?php
			if (!isset($_SESSION['email'])) {
				echo '<li>
                        <a href="../../authentification/login.php" id="test1">Se connecter</a>
                    </li>
                    <li>
                        <a href="enregistrement.php" id="test2">S\'inscrire</a>
                    </li>';
			}
			else {
                            if($_SESSION["codeTypeUtil"]==1){
                                echo '<li>
                        <a id="test1" href="listeRdvPatient.php">' . $_SESSION['prenom'] . '</a>';
                            }
                            else{
                                echo '<li>
                        <a id="test1" href="listeRdvMedecin.php">' . $_SESSION['prenom'] . '</a>';
                            }
				
                    echo '</li>
                    <li>
                        <a href="../../authentification/traitementDeco.php" id="test2">Se déconnecter</a>
                    </li>';
			}
			?>
            </ul>
        </div>
        <h1 id="enTete">Liste des rendez-vous d'un patient</h1>
        <?php
        include("../test/mesFonctions.php");
        echo menu();
        ?>
        <div id="contenu">
            <form action="listeRdvPatient.php" method="post">
                <label for="id">Entrez votre id :</label>
                <input type="text" name="id"/>
                <input type="submit" name="valider" value="confirmer"/>
            </form>


        </div>
    </body>
</html>
