<!DOCTYPE html>
<?php
session_start();
include("../test/mesFonctions.php");
$unPdo = Connexion();
?>
<html>
    <head>
        <title>Liste des Rendez-vous Patient</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../style.css"/>
        <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 
    </head>
    <body>
        <header>
            <div id="titre">H<a href="http://humourtop.com/les-meilleurs-gifs-droles-de-2013/Simba_dead.gif" id="important">ô</a>pital LuDaJu</div>
            <div id="connexion">
                <ul>
                    <?php
			if (!isset($_SESSION['email'])) {
				echo '<li>
                        <a href="../../authentification/login.php" id="test1">Se connecter</a>
                    </li>
                    <li>
                        <a href="enregistrement.php" id="test2">S\'inscrire</a>
                    </li>';
			}
			else {
                            if($_SESSION["codeTypeUtil"]==1){
                                echo '<li>
                        <a id="test1" href="listeRdvPatient.php">' . $_SESSION['prenom'] . '</a>';
                            }
                            else{
                                echo '<li>
                        <a id="test1" href="listeRdvMedecin.php">' . $_SESSION['prenom'] . '</a>';
                            }
				
                    echo '</li>
                    <li>
                        <a href="../../authentification/traitementDeco.php" id="test2">Se déconnecter</a>
                    </li>';
			}
			?>
                </ul>
            </div>

        </header>
        <?php
        include_once("../test/mesFonctions.php");
        echo menu();
        ?>
        <div id="contenu">
            <h1 id="enTete">Liste des rendez-vous d'un patient</h1>
            <?php
            $email = $_SESSION["email"];
            $lesPatients = listeRdvPatient($unPdo, $email);

            echo'<table>';
            echo'<tr><th class="rdv">Nom</th><th class="rdv">Prenom</th><th class="rdv">Date Heure Rdv</th></tr>';
            foreach ($lesPatients as $unPatient) {
                echo'<tr><td class="rdv">' . $unPatient['nom'] . '</td><td class="rdv">' . $unPatient['prenom'] . '</td><td class="rdv">' . $unPatient['dateheurerdv'] . '</td></tr>';
            }
            echo'</table>';
            ?>

        </div>
        <footer>
            <center>
                <table>
                    <tr><th class="foot">Facebook</th><th class="foot">Twitter</th><th class="foot">Google+</th></tr>
                    <tr><th class="tdFoot"><a href="https://fr-fr.facebook.com/"><img class="test" src="../../images/facebook.png"/></a></th><th class="tdFoot"><a href="https://twitter.com/?lang=fr"><img class="test" src="../../images/twitter.png"/></a></th><th class="tdFoot"><a href="https://media.tenor.co/images/1f034d4f7d72a87a3167aff1395d5143/tenor.gif"><img class="test" src="../../images/google.png"/></a></th></tr>
                </table>
                <div>© 2017 LuDaJu. Tous droits réservés.</div>
            </center>
        </footer>
    </body>
</html>