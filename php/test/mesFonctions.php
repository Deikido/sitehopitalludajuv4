<?php
if (!isset($_SESSION['codeTypeUtil'])) {
    $_SESSION['codeTypeUtil'] = 0;
}
function Connexion() {

    $dsn = 'mysql:dbname=bdd_hopital_ludaju;host=localhost';
    $user = 'root';
    $password = '';

    try {
        $dbh = new PDO($dsn, $user, $password);
        $dbh->exec("SET CHARACTER SET utf8");
        return $dbh;
    } catch (PDOException $e) {
        echo 'Connexion échouée : ' . $e->getMessage();
    }
}

function listeRdvPatient($unPdo, $email) {
    $unPdoStatement = $unPdo->prepare("SELECT utilisateur.nom, utilisateur.prenom, "
            . "rdv.dateheurerdv FROM utilisateur INNER JOIN rdv "
            . "ON utilisateur.email=rdv.emailMedecin "
            . "WHERE rdv.emailPatient=:email");
    $ok = $unPdoStatement->execute(array('email' => $email));
    if ($ok) {
        $lesLignes = $unPdoStatement->fetchAll();
    } else {
        $lesLignes = array();
    }
    return $lesLignes;
}

function afficherMedecin($listeMedecin) {
    $monPdoStatement = $listeMedecin->prepare("SELECT email,nom "
            . "FROM utilisateur WHERE codeTypeUtil = 2");
    $ok = $monPdoStatement->execute();

    if ($ok) {
        $lesLignes = $monPdoStatement->fetchAll();
    } else {
        $monPdoStatement->closeCursor();
    }
    return $lesLignes;
}

function afficherRdvMed($listeRdv, $emailMedecin) {
    $monPdoStatement = $listeRdv->prepare("SELECT idrdv, dateheurerdv, datepriserdv, emailPatient "
            . "FROM rdv WHERE emailMedecin = "
            . ":emailMedecin");
    $ok = $monPdoStatement->execute(array('emailMedecin' => $emailMedecin));

    if ($ok) {
        $lesRdv = $monPdoStatement->fetchAll();
    } else {
        $monPdoStatement->closeCursor();
    }
    return $lesRdv;
}

function Redirect($url) {
    header('Location: ' . $url, true, 303);
    die();
}

function chargerListeServices($objPdo) {
    $stmt = $objPdo->prepare("select * from service;");
    $stmt->execute();
    $services = $stmt->fetchAll();
    $stmt->closeCursor();
    return $services;
}

function chargerListeMedecins($objPdo) {
    $stmt = $objPdo->prepare("select * from utilisateur WHERE codeTypeUtil = 2;");
    $stmt->execute();
    $medecins = $stmt->fetchAll();
    $stmt->closeCursor();
    return $medecins;
}

function js_str($s) {
    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
}

function js_array($array) {
    $temp = array_map('js_str', $array);
    return '[' . implode(',', $temp) . ']';
}

function menu() {
    $menu = '<div id="menu">
	<div class="optMenu"><a href="../../index.php">Accueil</a></div>
	<div id="S1">
	<a href="Pediatrie.php">Pédiatrie</a>
		<p id="SS1">
			<a href="Cardiologie.php">-Cardiologie</a><br>
			<a href="Neurologie.php">-Neurologie</a><br>
			<a href="Nephrologie.php">-Nephrologie</a>
		</p>
	</div>
	
	<div id="S2">
	<a href="chirurgie.php">Chirurgie</a>
	<p id="SS2">
		<a href="chirurgieDigestive.php">-Chirurgie digestive</a><br>
		<a href="chirurgieOrthoTrauma.php">-Chirurgie orthopédique et traumatologique</a><br>
		<a href="neurochirurgie.php">-Neurochirurgie</a><br>
		<a href="chirurgieAmbulatoire.php">-Chirurgie ambulatoire</a>
	</p>
	</div>
		
	<div id="S3">
	<a href="Medecine.php">Médecine</a>
	<p id="SS3">
		<a href="Dermatologie.php">-Dermatologie</a><br>
		<a href="Endocrinologie.php">-Endocrinologie</a><br>
		<a href="Geriatrique.php">-Médecine gériatrique</a>
	</p>
	</div>
	
	<div class="optMenu"><a href="Urgences.php">Urgences</a></div>
	
	<div class="optMenu"><a href="Administration.php">Administration</a></div>
	
	<div class="optMenu"><a href="Contact.php">Contact/Accès</a></div>
	';
        if($_SESSION["codeTypeUtil"] != 2){
            $menu = $menu.'<div class="optMenu"><a href="demandeRendezVous.php">Rendez-vous</a></div>';
        }
        $menu = $menu.'</div>';
    return $menu;
}

function menuI() {
    $menu = '<div id="menu">
	<div class="optMenu"><a href="index.php">Accueil</a></div>
	<div id="S1">
	<a href="php/script/Pediatrie.php">Pédiatrie</a>
		<p id="SS1">
			<a href="php/script/Cardiologie.php">-Cardiologie</a><br>
			<a href="php/script/Neurologie.php">-Neurologie</a><br>
			<a href="php/script/Nephrologie.php">-Nephrologie</a>
		</p>
	</div>
	
	<div id="S2">
	<a href="php/script/chirurgie.php">Chirurgie</a>
	<p id="SS2">
		<a href="php/script/chirurgieDigestive.php">-Chirurgie digestive</a><br>
		<a href="php/script/chirurgieOrthoTrauma.php">-Chirurgie orthopédique et traumatologique</a><br>
		<a href="php/script/neurochirurgie.php">-Neurochirurgie</a><br>
		<a href="php/script/chirurgieAmbulatoire.php">-Chirurgie ambulatoire</a>
	</p>
	</div>
		
	<div id="S3">
	<a href="php/script/Medecine.php">Médecine</a>
	<p id="SS3">
		<a href="php/script/Dermatologie.php">-Dermatologie</a><br>
		<a href="php/script/Endocrinologie.php">-Endocrinologie</a><br>
		<a href="php/script/Geriatrique.php">-Médecine gériatrique</a>
	</p>
	</div>
	
	<div class="optMenu"><a href="php/script/Urgences.php">Urgences</a></div>
	
	<div class="optMenu"><a href="php/script/Administration.php">Administration</a></div>
	
	<div class="optMenu"><a href="php/script/Contact.php">Contact/Accès</a></div>';
	
	if($_SESSION["codeTypeUtil"] != 2){
            $menu = $menu.'<div class="optMenu"><a href="php/script/demandeRendezVous.php">Rendez-vous</a></div>';
        }
        $menu = $menu.'</div>';
    return $menu;
}