<?php
	session_start();
	if(isset($_SESSION["login"]))
	{
		unset($_SESSION["login"]);
	}
	header('Location: ' . "test.php", true, 303);
    die();
?>