<?php session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>BTS SIO</title>
        <meta charset="UTF-8">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0"/>-->
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 
    </head>
    <body>
        <header>
            <div id="titre">H<a href="http://humourtop.com/les-meilleurs-gifs-droles-de-2013/Simba_dead.gif" id="important">ô</a>pital LuDaJu</div>
            <div id="connexion">
                <ul>
                    <?php
                    if (!isset($_SESSION['email'])) {
                        echo '<li>
                        <a href="authentification/login.php" id="test1">Se connecter</a>
                    </li>
                    <li>
                        <a href="php/script/enregistrement.php" id="test2">S\'inscrire</a>
                    </li>';
                    } else {
                        if ($_SESSION["codeTypeUtil"] == 1) {
                            echo '<li>
                        <a id="test1" href="php/script/listeRdvPatient.php">' . $_SESSION['prenom'] . '</a>';
                        } else {
                            echo '<li>
                        <a id="test1" href="php/script/listeRdvMedecin.php">' . $_SESSION['prenom'] . '</a>';
                        }

                        echo '</li>
                    <li>
                        <a href="authentification/traitementDeco.php" id="test2">Se déconnecter</a>
                    </li>';
                    }
                    ?>
                </ul>
            </div>
        </header>
<?php
include("php/test/mesFonctions.php");
echo menuI();
?>
        <div id="contenu">
            <h1 id='Presentation'>Bienvenue sur le site de l'hôpital LuDaJu</h1>
            <center><img id="image_accueil" src="images/img_hopital.jpg"/></center>
            <p>loin de moi l'idée de graver dans le marbre de tailler dans une écorce d'arbre loin de moi l'idée de suggérer que je m'en moque que je n'en ai rien à faire que guère je ne m'en soucie loin de moi ces folies mais je m'échine depuis octobre et pourquoi donc depuis début octobre même et qui m'aime me suive depuis octobre depuis ce même dernier octobre le trois du mois je crois depuis ce temps-là depuis trois mois depuis trois mois et une semaine je m'échine ailleurs et le très long texte n'a pas avancé d'un poil pas beaucoup sans doute est-ce mon côté velléitaire qui ne cesse de me jouer des tours et les méandres du très long texte se sont figés comme une gelée le long des parois d'un bocal de verre et je vitupère contre mes essais éphémères mon tempérament affreusement velléitaire et ce teint d'albâtre qui n'est pas le mien comme je voudrais qu'il fût d'albâtre ou d'ébène ou autrement même sans métaphore mais au moins qu'il ait quelque tenue que mon visage sans retenue puisse soudain passer pour un tissu une pierre un songe soit en quelque sorte un tableau fasse tableau mais ce n'est pas le cas même ce mot albâtre jeté au visage jeté tout à trac sur la page en haut de page ce mot me défigure ne me figure pas ne me représente pas ne figure rien de ce que je suis de ce que je pense être et je suis encore et toujours circonspect dans le doute et ce mot n'apporte rien aucune réponse et donc toujours je me jette à la figure ces accusations comme des bouteilles non pas à la mer mais bien dans la gueule oui je me donne des coups de bouteille tessons épars sur le parquet et mes joues ensanglantées enfin que ce soit ou non métaphore que le mot d'albâtre me figure ou non je prends ces coups ces reproches en plein visage et je m'accuse d'être velléitaire aussi bien sûr pour trop entreprendre je lance cent feux il est normal qu'un certain nombre des foyers meure et même ne démarre qu'à peine avant de s'achever dans un bruit de feuilles mouillées de bois mort de bois trop vert encore pour prendre tout cela encore métaphore et toujours métaphore peut-être est-ce le mot albâtre qui appelle autant de métaphores ou bien les conditions d'écriture du très long texte que par facétie ou encore autodérision je pourrais être tenté de rebaptiser très long texte interrompu et l'adjectif interrompu ici au milieu de la ligne interrompt mes songes interrompt le torrent de sornettes lance d'autres tirades propose peut-être d'autres charades mais pour mieux me ramener vers le rivage bourbeux où je ne cesse de me lancer ces reproches à la figure velléitaire velléitaire et me voici encore à ne pas même essayer de me justifier moi-même de tout cela feux mal éteints et feux qui n'ont jamais pris aussi me trouvé-je vingt vaines justifications improbables même si certaines sont justes par ailleurs comme dans le cas du projet de traduire régulièrement et pensais-je au début au moins une fois par semaine un poème et qui s'est enlisé après à peine trois ou quatre tracasseries mais cela reprendra parfois aussi depuis début octobre le trois je crois suspendu à ce mot d'albâtre depuis le trois octobre le trois je crois je me disais que pour être interrompu ou inachevé le très long texte recelait de vraies possibilités et qu'il suffisait suffirait eût suffi de s'y remettre et la machine reprendrait du galon non là cette image-là ne va pas je mélange les formules croise les figures de style et donc je pensais qu'il me faudrait toutes proportions gardées envisager ces carnets comme Paul Valéry travaillant régulièrement et sans espoir d'en finir jamais chaque matin à ses Cahiers désormais regroupés en deux tomes en Pléiade et que j'ai dévorés consultés admirés lus compulsés longuement naguère mais il faudrait dire jadis ou balancer entre les deux lus disons entre 1993 et 1997 et donc toutes proportions gardées je me verrais bien ainsi à reprendre tel chantier interrompu trois mois et le faisant avancer un petit peu mais enfin ce n'est pas possible il ne va pas se comparer à Paul Valéry l'autre oiseux oisif ex-oisien de surcroît ancien oisien into the bargain non il ne va pas se comparer à Paul Valéry tout de même alors que seulement et il nous l'a dit même avec métaphores tout le tintouin oui oui noir sur blanc dit ce n'est rien d'autre qu'un affreux</p>
        </div>

        <footer>
            <center>
                <table>
                    <tr><th class="foot">Facebook</th><th class="foot">Twitter</th><th class="foot">Google+</th></tr>
                    <tr><th class="tdFoot"><a href="https://fr-fr.facebook.com/"><img class="test" src="images/facebook.png"/></a></th><th class="tdFoot"><a href="https://twitter.com/?lang=fr"><img class="test" src="images/twitter.png"/></a></th><th class="tdFoot"><a href="https://media.tenor.co/images/1f034d4f7d72a87a3167aff1395d5143/tenor.gif"><img class="test" src="images/google.png"/></a></th></tr>
                </table>
                <div>© 2017 LuDaJu. Tous droits réservés.</div>
            </center>
        </footer>
    </body>
</html>
